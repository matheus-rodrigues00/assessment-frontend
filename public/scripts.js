const hamburguerButton = document.getElementById('hamburguer-nav-button');
const mainSidebar = document.querySelector('.sidebar-pagination-container');
const searchButton = document.getElementById('search-mobile-button');
const searchInput = document.querySelector('.search-container');

if (window.location.href == 'http://localhost:8888/index.html') {
  mainSidebar.classList.add('active');
}

hamburguerButton.addEventListener('click', () => {
  if (searchInput.classList.contains('active')) {
    searchInput.classList.toggle('active');
  }
  hamburguerButton.classList.toggle('active');
  mainSidebar.classList.toggle('active');
});

searchButton.addEventListener('click', () => {
  searchInput.classList.toggle('active');
  if (mainSidebar.classList.contains('active')) {
    hamburguerButton.classList.toggle('active');
    mainSidebar.classList.toggle('active');
  }
});

// ITEMS CODE
// DINAMIC TITLE ELEMENTS
const pageTitle = document.getElementById('page-title');
const htmlTitle = document.getElementById('html-title');
const spanTitle = document.querySelector('.actual-page');

const main = document.querySelector('.product-main');
const productContainer = document.querySelector('.products-container');
const APIURL = 'http://localhost:8888/api/V1/categories/';
const orderSelect = document.getElementById('order-select');
const filterByClothingButton = document.getElementById('clothes-btn');
const filterByShoesButton = document.getElementById('shoes-btn');
const filterByAcessoriesButton = document.getElementById('accessories-btn');
const filterByRunningShoesButton = document.getElementById('running-shoes-btn');
const filterByWalkingShoesButton = document.getElementById('walking-shoes-btn');
const filterByCasualButton = document.getElementById('casual-btn');
const filterBySocialButton = document.getElementById('social-btn');

// FILTER COLOR BUTTONS
const orangeColorButton = document.getElementById('orange-btn');
const yellowColorButton = document.getElementById('yellow-btn');
const greenColorButton = document.getElementById('green-btn');

yellowColorButton.addEventListener('click', () => {
  getProducts(page, '', false, false, false, false, 'Amarela');
});

orangeColorButton.addEventListener('click', () => {
  getProducts(page, '', false, false, false, false, 'Laranja');
});

greenColorButton.addEventListener('click', () => {
  getProducts(page, '', false, false, false, false, 'Verde');
});

const createProductCard = (productPrice, productImage, productName) => {
  const cardHtml = `
    <div class="product-card">
      <img src="${productImage}" alt="${productName}" class="product-image" />
      <div>
        <h3 class="product-name">${productName}</h3>
        <div class="price-container">${productPrice}</div>
        <button class="buy-button">Comprar</button>
      </div>
    </div>
    `;
  productContainer.innerHTML += cardHtml;
};

async function getProducts(
  sectionId,
  selectValue,
  isClothes,
  isForRun,
  isCasual,
  isSocial,
  color
) {
  try {
    productContainer.innerHTML = '';
    const { data } = await axios(APIURL + 'list');
    data.items.forEach(async (val, i) => {
      // Dinamic title of the page
      if (sectionId == i + 1) {
        pageTitle.innerHTML = val.name;
        htmlTitle.innerHTML = val.name;
        spanTitle.innerHTML = val.name;
      } else if (isClothes) {
        pageTitle.innerHTML = 'Roupas';
      }

      const { data } = await axios(APIURL + sectionId);

      if (selectValue == 'higherPrice') {
        data.items.sort((a, b) => b.price - a.price);
      } else if (selectValue == 'lowPrice') {
        data.items.sort((a, b) => a.price - b.price);
      }

      data.items.forEach((v, idx) => {
        console.log(v.specialPrice);
        v.price = v.specialPrice
          ? `
          <span class="product-offer-price">R$${formatPrice(v.price)}</span>
          <span class="product-price">R$${formatPrice(v.specialPrice)}</span>`
          : `<span class="product-price">R$${formatPrice(v.price)}</span>`;
        if (sectionId == val.id) {
          if (isForRun && v.name.indexOf('Corrida') >= 0) {
            createProductCard(v.price, v.image, v.name);
          } else if (isCasual && v.name.indexOf('Casual') >= 0) {
            createProductCard(v.price, v.image, v.name);
          } else if (isSocial && v.name.indexOf('Social') >= 0) {
            createProductCard(v.price, v.image, v.name);
          } else if (v.name.indexOf(color) >= 0) {
            createProductCard(v.price, v.image, v.name);
          } else if (!isForRun && !isCasual && !isSocial && !color) {
            createProductCard(v.price, v.image, v.name);
          }
        }
      });
    });
  } catch (e) {
    console.log('Error, we could not access the products api');
    return;
  }
}

// ORDER SELECT
orderSelect.addEventListener('input', (e) => {
  getProducts(page, e.target.value);
});

const verifyPage = () => {
  if (window.location.href.indexOf('shoes') >= 0) {
    page = 3;
  } else if (window.location.href.indexOf('pants') >= 0) {
    page = 2;
  } else if (window.location.href.indexOf('shirts') >= 0) {
    page = 1;
  }
};

filterByClothingButton.addEventListener('click', () => {
  getProducts(1, '', true);
  getProducts(2, '', true);
  page = 1;
});

filterByShoesButton.addEventListener('click', () => {
  page = 3;
  getProducts(page);
});

filterByAcessoriesButton.addEventListener('click', () => {
  getProducts(4);
});

filterByRunningShoesButton.addEventListener('click', () => {
  getProducts(page, '', false, true);
});

filterByWalkingShoesButton.addEventListener('click', () => {
  getProducts(page, '', false, true);
});

filterByCasualButton.addEventListener('click', () => {
  getProducts(page, '', false, false, true);
});

filterBySocialButton.addEventListener('click', () => {
  getProducts(page, '', false, false, false, true);
});

// INITIALIZATION
let page;
verifyPage();
getProducts(page);

function formatPrice(price) {
  price += '';
  if (price.indexOf('.') >= 0) {
    console.log(price);
    price = price.split('.');
    price[1] = (price[1] + '0').slice(-2);
    return price.join(',');
  }
  return price.substring(0, price.length) + ',00';
}
