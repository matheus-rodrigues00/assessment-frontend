# Documentação do Projeto
- Nesse projeto utilizei o scss para a estilização. De forma enxuta, não utlizei muitos arquivos. Em certos trechos de código do scss é possível notar repetição, deixei dessa forma para que a leitura fosse mais simples, prefiro um código razoavelmente enxuto e legível do que um código muito enxuto e denso. 
- No JavaScript foi utilizado a biblioteca Axios, Axios é um cliente HTTP simples baseado em promessas para o navegador e para o node.js. Axios fornece uma biblioteca simples de usar em um pacote pequeno com uma grande interface, acho o Axios melhor do que simplesmente o fetch para requisições javascript. Tanto no vanilla js quanto no React. 
- Utilizei tags HTML específicas para melhorar o SEO da página, prestei bastante atenção aos textos "alt" também, isso além de melhorar o SEO também melhora a acessibilidade.

## Como iniciar o projeto:
1. Instale o npm
2. Clone o projeto com a linha: git clone https://matheus-rodrigues00@bitbucket.org/matheus-rodrigues00/assessment-frontend.git
3. Instale as dependências com "npm install"
4. Rode a aplicação com "npm start"
5. Acesse http://localhost:8888/index.html
